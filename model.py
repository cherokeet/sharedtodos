from google.appengine.ext import ndb

class List(ndb.Model):
	owner = ndb.StringProperty()
	subscribers = ndb.StringProperty(repeated=True)
	name = ndb.StringProperty(indexed=False)

class ListItem(ndb.Model):
	label = ndb.StringProperty(indexed=False)
	completed = ndb.BooleanProperty(indexed=False)
	timestamp = ndb.DateTimeProperty(auto_now=True)


	