import bottle
import logging, sys
from model import List, ListItem

bottle.debug(True)

from google.appengine.ext import ndb

@bottle.post('/lists')
def post_list():
	entity = List(**bottle.request.json)
	key = entity.put()
	return key.urlsafe()

@bottle.get('/lists')
def get_lists():
	email = bottle.request.query['email']
	entities = List.query(ndb.OR(List.owner == email, List.subscribers.IN([email]))).fetch()
	payload = {"items": []}
	for entity in entities:
		payload['items'].append(entity.to_dict())
	return payload

@bottle.get('/lists/<list_id>')
def get_list(list_id):
	entity = ndb.Key(urlsafe=list_id).get()
	return entity.to_dict()

@bottle.put('/lists/<list_id>')
def put_list(list_id):
	key = ndb.Key(urlsafe=list_id)
	entity = key.get()
	for attr, value in bottle.request.json.iteritems():
		setattr(entity, attr, value)
	entity.put()
	return key.urlsafe()

@bottle.post('/lists/<list_id>/items')
def post_list_item(list_id):
	entity = ListItem(parent=ndb.Key(urlsafe=list_id), **bottle.request.json)
	key = entity.put()
	return key.urlsafe()

@bottle.get('/lists/<list_id>/items')
def get_list_items(list_id):
	parent_key = ndb.Key(urlsafe=list_id)
	entities = ListItem.query(ancestor=parent_key).fetch()
	payload = {"items": []}
	for entity in entities:
		payload['items'].append(entity.to_dict(exclude=['timestamp']))
	return payload

@bottle.get('/items/<item_id>')
def get_list_item(item_id):
	entity = ndb.Key(urlsafe=item_id).get()
	return entity.to_dict(exclude=['timestamp'])

@bottle.put('/items/<item_id>')
def put_list(item_id):
	key = ndb.Key(urlsafe=item_id)
	entity = key.get()
	for attr, value in bottle.request.json.iteritems():
		setattr(entity, attr, value)
	entity.put()
	return key.urlsafe()

app = bottle.app()
print("READY")
